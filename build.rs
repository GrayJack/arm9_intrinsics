// use std::env;

use cc;

static ARCH_FLAGS: &[&str] = &["-mthumb-interwork", "-mcpu=arm946e-s", "-msoft-float"];

fn gcc_config() -> cc::Build {
    let mut config = cc::Build::new();
    for flag in ARCH_FLAGS {
        config.flag(flag);
    }
    config.flag("-fno-strict-aliasing").flag("-std=c11");
    config
}

fn main() {
    // println!("cargo:rerun-if-env-changed=C9_PROG_TYPE");
    //
    // // Make sure the requested program actually exists
    // let prog = env::var("C9_PROG_TYPE").unwrap();
    // let modfile = include_str!("src/programs/mod.rs");
    // let start = modfile.find("define_programs!(").unwrap();
    // let end = modfile[start..].find(");").unwrap() + start;
    // modfile[start..end].find(&format!("\"{}\"", prog))
    //     .expect(&format!("Could not find program `{}`!", prog));

    println!("cargo:rerun-if-changed=src/start.s");
    println!("cargo:rerun-if-changed=src/exception.s");
    println!("cargo:rerun-if-changed=src/caches.s");

    gcc_config()
        .file("src/exception.s")
        .file("src/caches.s")
        // .file("src/start.s")
        .compile("libbasic.a");

    // println!("cargo:rerun-if-changed=src/armwrestler.s");
    // println!("cargo:rerun-if-changed=src/cache_benchers.s");
    //
    // gcc_config()
    //     .flag("-w")
    //     .file("src/programs/armwrestler.s")
    //     .file("src/programs/cache_benchers.s")
    //     .compile("libtestasm.a");
    //
    // gcc_config()
    //     .flag("-w")
    //     .file("src/programs/os/entry.s")
    //     .compile("libosasm.a");
}
