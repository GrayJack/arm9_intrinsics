//! Bare-metal exception handlers for the platform
use core::fmt::{self, Display};

use crate::io::irq::{interrupts, IrqRegister};

extern "C" {
    static ldr_pc_pc_neg4: u32;
    fn wrap_handle_irq();
    fn wrap_handle_fiq();
    fn wrap_handle_swi();
    fn wrap_handle_und();
    fn wrap_handle_pre();
    fn wrap_handle_dta();

    /// Wait for a interrupt to happen.
    pub fn wait_for_interrupt();

    /// Disable interrupts and returns if it was previously enabled.
    fn disable_interrupts() -> bool;

    /// Enable interrups.
    fn enable_interrupts();
}

/// Run a callable (function or closure) with disabled interrupts.
pub fn without_interrupts<T, F: FnOnce() -> T>(f: F) -> T {
    let was_enabled = unsafe { disable_interrupts() };
    let ret = f();
    if was_enabled {
        unsafe { enable_interrupts() };
    }
    ret
}

/// Initialize all interrupts.
#[no_mangle]
pub extern "C" fn init_interrupts() {
    without_interrupts(|| {
        static VECTOR_MAPPINGS: [(u32, unsafe extern "C" fn()); 6] = [
            (0x08000000, wrap_handle_irq),
            (0x08000008, wrap_handle_fiq),
            (0x08000010, wrap_handle_swi),
            (0x08000018, wrap_handle_und),
            (0x08000020, wrap_handle_pre),
            (0x08000028, wrap_handle_dta),
        ];

        for &(addr, handler) in VECTOR_MAPPINGS.iter() {
            unsafe {
                *(addr as *mut u32) = ldr_pc_pc_neg4;
                *((addr + 4) as *mut u32) = handler as u32;
            }
        }

        IrqRegister::Enabled.disable(!0);
        IrqRegister::Pending.clear_all();
    })
}

/// Handler function type.
pub type HandlerFn = fn();

/// A list for handles for all IRQ interrupts.
static mut HANDLER_LIST: [(u32, [Option<HandlerFn>; 4]); 30] = [
    (interrupts::DMAC_1_0, [None, None, None, None]),
    (interrupts::DMAC_1_1, [None, None, None, None]),
    (interrupts::DMAC_1_2, [None, None, None, None]),
    (interrupts::DMAC_1_3, [None, None, None, None]),
    (interrupts::DMAC_1_4, [None, None, None, None]),
    (interrupts::DMAC_1_5, [None, None, None, None]),
    (interrupts::DMAC_1_6, [None, None, None, None]),
    (interrupts::DMAC_1_7, [None, None, None, None]),
    (interrupts::TIMER_0, [None, None, None, None]),
    (interrupts::TIMER_1, [None, None, None, None]),
    (interrupts::TIMER_2, [None, None, None, None]),
    (interrupts::TIMER_3, [None, None, None, None]),
    (interrupts::PXI_SYNC, [None, None, None, None]),
    (interrupts::PXI_NOT_FULL, [None, None, None, None]),
    (interrupts::PXI_NOT_EMPTY, [None, None, None, None]),
    (interrupts::AES, [None, None, None, None]),
    (interrupts::SDIO_1, [None, None, None, None]),
    (interrupts::SDIO_1_ASYNC, [None, None, None, None]),
    (interrupts::SDIO_3, [None, None, None, None]),
    (interrupts::SDIO_3_ASYNC, [None, None, None, None]),
    (interrupts::DEBUG_RECV, [None, None, None, None]),
    (interrupts::DEBUG_SEND, [None, None, None, None]),
    (interrupts::RSA, [None, None, None, None]),
    (interrupts::CTR_CARD_1, [None, None, None, None]),
    (interrupts::CTR_CARD_2, [None, None, None, None]),
    (interrupts::CGC, [None, None, None, None]),
    (interrupts::CGC_DET, [None, None, None, None]),
    (interrupts::DS_CARD, [None, None, None, None]),
    (interrupts::DMAC_2, [None, None, None, None]),
    (interrupts::DMAC_2_ABORT, [None, None, None, None]),
];

#[derive(Debug)]
pub enum Error {
    InvalidInterrupt,
    HandlersFull,
    NotFound,
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::InvalidInterrupt => write!(f, "Invalid interrupt"),
            Self::HandlersFull => write!(f, "All handlers are full"),
            Self::NotFound => write!(f, "Handler not found"),
        }
    }
}

/// Find handler for the `interrupt_type`.
pub fn find_handler<'a>(
    interrupt_type: u32, val: Option<HandlerFn>,
) -> Result<&'a mut Option<HandlerFn>, Error> {
    without_interrupts(|| {
        let pos = match unsafe { HANDLER_LIST.iter() }.position(|&x| x.0 == interrupt_type) {
            Some(x) => x,
            None => return Err(Error::InvalidInterrupt),
        };
        let found_pos = match unsafe { HANDLER_LIST[pos].1.iter() }.position(|&x| x == val) {
            Some(x) => x,
            None => return Err(Error::NotFound),
        };
        Ok(unsafe { &mut HANDLER_LIST[pos].1[found_pos] })
    })
}

/// Register a given `handler` for a `interrupt_type`.
pub fn register_handler(interrupt_type: u32, handler: HandlerFn) -> Result<(), Error> {
    *find_handler(interrupt_type, None)? = Some(handler);
    Ok(())
}

/// Unregister a `handler` from a `interrupt_type` if it exist.
pub fn unregister_handler(interrupt_type: u32, handler: HandlerFn) -> Result<(), Error> {
    *find_handler(interrupt_type, Some(handler))? = None;
    Ok(())
}

#[no_mangle]
pub extern "C" fn handle_irq() {
    let pending_interrupts = IrqRegister::Pending.read() & IrqRegister::Enabled.read();

    let found = unsafe { HANDLER_LIST.iter() }.find(|&x| pending_interrupts & x.0 != 0);
    let &(interrupt, handlers) = match found {
        Some(x) => x,
        None => {
            // Should never occur
            IrqRegister::Pending.clear(pending_interrupts);
            return;
        },
    };

    IrqRegister::Pending.clear(interrupt);

    for handler in handlers.iter().filter_map(|&x| x) {
        handler();
    }
}

/// Function type for SWI Handler.
pub type SwiHandlerFn = fn(u32, bool, &mut [u32; 15], &mut u32);

/// Handler for SWI.
static mut SWI_HANDLER: Option<SwiHandlerFn> = None;

/// Register a `handler` as SWI handler.
pub fn register_swi_handler(handler: SwiHandlerFn) -> Result<(), &'static str> {
    unsafe {
        if SWI_HANDLER.is_none() {
            SWI_HANDLER = Some(handler);
            Ok(())
        } else {
            Err("Attempted to override existing SWI handler")
        }
    }
}

/// Unregister SWI handler.
pub fn unregister_swi_handler() { unsafe { SWI_HANDLER = None }; }

#[no_mangle]
pub extern "C" fn handle_swi(swi_index: u32, is_thumb: u32, regs: *mut [u32; 15], pc: *mut u32) {
    if let Some(h) = unsafe { SWI_HANDLER } {
        h(swi_index, is_thumb != 0, unsafe { &mut *regs }, unsafe { &mut *pc });
    } else {
        panic!("Handling software interrupt {:02X} failed!", swi_index);
    }
}

#[no_mangle]
pub extern "C" fn handle_und(addr: u32) {
    panic!("Undefined instruction @ 0x{:X}!", addr);
}

#[no_mangle]
pub extern "C" fn handle_pre(addr: u32, lr: u32, sp: u32) {
    use alloc::string::String;
    use core::{fmt::Write, slice};

    let stack = unsafe { slice::from_raw_parts(sp as *mut u32, 6 * 4) };
    let mut stack_str = String::new();
    for i in 0..6 {
        let _ = write!(
            stack_str,
            "    {:08X}, {:08X}, {:08X}, {:08X}\n",
            stack[i * 4 + 0],
            stack[i * 4 + 1],
            stack[i * 4 + 2],
            stack[i * 4 + 3]
        );
    }
    let stack_str = "";
    let _ = sp;

    panic!("Prefetch abort @ 0x{:X}! lr=0x{:X}, stack=\n{}", addr, lr, stack_str);
}

#[no_mangle]
pub extern "C" fn handle_dta(addr: u32) {
    panic!("Data abort @ 0x{:X}!", addr);
}
