//! Inter Integrated Circuit (I²C) module and API
//!
//! Detailed information on [3dbrew](https://www.3dbrew.org/wiki/I2C_Registers)
use core::ptr;

pub const I2C_BUS_1_BASE: u32 = 0x10161000;
pub const I2C_BUS_2_BASE: u32 = 0x10144000;
pub const I2C_BUS_3_BASE: u32 = 0x10148000;

/// I²C Devices on the 3DS family products.
///
/// Aditional information on [3dbrew](https://www.3dbrew.org/wiki/I2C_Registers#I2C_Devices)
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Device {
    /// Micro Controler Unit (MCU). Maybe power management (same device addr as the DSi
    /// power-management).
    Mcu0 = 0x00,
    /// Camera 0.
    Cam0 = 0x01,
    /// Camera 1.
    Cam1 = 0x02,
    /// Micro Controler Unit (MCU). For sure power management.
    Mcu1 = 0x03,
    /// Camera 2.
    Cam2 = 0x04,
    /// Liquid Crystal Display 0 (LCD0).
    Lcd0 = 0x05,
    /// Liquid Crystal Display 1 (LCD1).
    Lcd1 = 0x06,
    /// Unknown
    Deb0 = 0x07,
    /// Unknown
    Deb1 = 0x08,
    /// Human Interface Device 0 (HID0). Unknown
    Hid0 = 0x09,
    /// Human Interface Device 1 (HID1). Gyroscope
    Hid1 = 0x0A,
    /// Human Interface Device 2 (HID2). Unknown
    Hid2 = 0x0B,
    /// Human Interface Device 3 (HID3). Debug Pad.
    Hid3 = 0x0C,
    /// InfraRed 0.
    Ir0  = 0x0D,
    /// EEPROM. Only present on dev units.
    Eep  = 0x0E,
    /// Near Field Communication (NFC). Only exist on New3DS/New2DS models.
    Nfc  = 0x0F,
    /// QTM. Only exist on New3DS/New2DS models.
    Qtm  = 0x10,
    /// Infra Red 1.
    Ir1  = 0x11,
}

impl Device {
    /// Get the device id.
    pub const fn device_id(self) -> u8 { self as u8 }

    /// Get the ID of the BUS for the `Device`.
    pub const fn bus_id(self) -> u8 {
        use Device::*;
        match self {
            Mcu0 | Cam0 | Cam1 | Qtm => 1,
            Mcu1 | Cam2 | Lcd0 | Lcd1 | Deb0 | Deb1 | Nfc => 2,
            Hid0 | Hid1 | Hid2 | Hid3 | Ir0 | Eep | Ir1 => 3,
        }
    }

    /// Get the write address for the `Device`.
    pub const fn write_address(self) -> u8 {
        use Device::*;
        match self {
            Mcu0 => 0x4A,
            Cam0 => 0x7A,
            Cam1 => 0x78,
            Mcu1 => 0x4A,
            Cam2 => 0x78,
            Lcd0 => 0x2C,
            Lcd1 => 0x2E,
            Deb0 => 0x40,
            Deb1 => 0x44,
            Hid0 => 0xD6,
            Hid1 => 0xD0,
            Hid2 => 0xD2,
            Hid3 => 0xA4,
            Ir0 => 0x9A,
            Eep => 0xA0,
            Nfc => 0xEE,
            Qtm => 0x40,
            Ir1 => 0x54,
        }
    }

    /// Get the base adress for the `Device`.
    fn base(&self) -> u32 {
        match self.bus_id() {
            1 => I2C_BUS_1_BASE,
            2 => I2C_BUS_2_BASE,
            3 => I2C_BUS_3_BASE,
            _ => unreachable!(),
        }
    }

    /// Get `CNT` register of the device.
    pub fn cnt(self) -> CntInfo { CntInfo(self.raw_cnt()) }

    /// Get `DATA` register of the device in raw `u8` value.
    pub fn raw_data(self) -> u8 { self.read_register(I2CRegisterType::Data) }

    /// Get `CNT` register of the device in raw `u8` value.
    pub fn raw_cnt(self) -> u8 { self.read_register(I2CRegisterType::Cnt) }

    /// Get `CNTEX` register of the device in raw `u8` value.
    pub fn raw_cntex(self) -> u8 { self.read_register(I2CRegisterType::Cntex) }

    /// Get `SCL` register of the device in raw `u8` value.
    pub fn raw_scl(self) -> u8 { self.read_register(I2CRegisterType::Scl) }

    /// Read the value of the register of the `Device`.
    ///
    /// The `reg_type` specify what register to read from the device base address.
    #[inline(never)]
    pub fn read_register(self, reg_type: I2CRegisterType) -> u8 {
        let base = self.base();

        unsafe { ptr::read_volatile((base + reg_type as u32) as *const u8) }
    }

    /// Write the given `value` in the register of the `Device`.
    ///
    /// The `reg_type` specify what register to read from the device base address.
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    #[inline(never)]
    pub unsafe fn write_register(self, reg_type: I2CRegisterType, value: u8) {
        let base = self.base();

        ptr::write_volatile((base + reg_type as u32) as *mut u8, value)
    }

    /// Wait busy
    pub fn wait_busy(self) {
        let cnt = self.cnt();

        while cnt.is_running() {}
    }

    fn operation_result(self) -> Result<(), ()> {
        self.wait_busy();
        match self.cnt().ack() {
            Ack::Error => Err(()),
            Ack::Ok => Ok(()),
        }
    }

    fn halt_xfer(self) { unsafe { self.write_register(I2CRegisterType::Cnt, 0xC5) } }

    fn xfer_last_byte(self, is_reading: bool) {
        unsafe { self.write_register(I2CRegisterType::Cnt, (is_reading as u8) << 5 | 0xC1) }
    }

    fn xfer_byte(self, is_reading: bool) {
        unsafe { self.write_register(I2CRegisterType::Cnt, (is_reading as u8) << 5 | 0xC0) }
    }

    fn select_target(&self, reg: u8, is_reading: bool) -> Result<(), ()> {
        self.wait_busy();
        unsafe {
            self.write_register(I2CRegisterType::Data, self.write_address());
            self.write_register(I2CRegisterType::Cnt, 0xC2);
        }
        self.operation_result()?;

        self.wait_busy();
        unsafe {
            self.write_register(I2CRegisterType::Data, reg);
            self.write_register(I2CRegisterType::Cnt, 0xC0);
        }
        self.operation_result()?;

        if is_reading {
            self.wait_busy();
            unsafe {
                self.write_register(I2CRegisterType::Data, self.write_address() | 1);
                self.write_register(I2CRegisterType::Cnt, 0xC2);
            }
            self.operation_result()?;
        }

        Ok(())
    }

    /// Read `Device` `DATA` register in a given target `register` and return the value if
    /// all occurs fine.
    ///
    /// You can find `Device` registers docummented [here](https://www.3dbrew.org/wiki/I2C_Registers#I2C_Devices).
    pub fn read_byte(self, reg: u8) -> Result<u8, ()> {
        for _ in 0..8 {
            if self.select_target(reg, true).is_ok() {
                self.wait_busy();
                self.xfer_byte(true);
                self.wait_busy();
                self.halt_xfer();
                self.wait_busy();
                return Ok(self.read_register(I2CRegisterType::Data));
            }
            self.halt_xfer();
            self.wait_busy();
        }
        Err(())
    }

    /// Read `Device` `DATA` register in a given target `register` and put the data inside
    /// of `dest`.
    ///
    /// You can find `Device` registers docummented [here](https://www.3dbrew.org/wiki/I2C_Registers#I2C_Devices).
    pub fn read_bytes(self, register: u8, dest: &mut [u8]) -> Result<(), ()> {
        if dest.len() == 0 {
            return Ok(());
        }

        for _ in 0..8 {
            if self.select_target(register, true).is_ok() {
                for n in 0..(dest.len() - 1) {
                    self.wait_busy();
                    unsafe {
                        self.write_register(I2CRegisterType::Cnt, 0xF0);
                    }
                    self.wait_busy();
                    dest[n] = self.read_register(I2CRegisterType::Data);
                }

                self.wait_busy();
                self.xfer_last_byte(true);
                self.wait_busy();

                let dest_end = dest.len() - 1;
                dest[dest_end] = self.read_register(I2CRegisterType::Data);
                return Ok(());
            }
            self.wait_busy();
            self.halt_xfer();
            self.wait_busy();
        }
        Err(())
    }

    /// Write `value` in the `DATA` register in a given `Device` `register` target.
    ///
    /// You can find `Device` registers docummented [here](https://www.3dbrew.org/wiki/I2C_Registers#I2C_Devices).
    pub fn write_byte(self, register: u8, value: u8) -> Result<(), ()> {
        for _ in 0..8 {
            if self.select_target(register, false).is_ok() {
                self.wait_busy();
                unsafe {
                    self.write_register(I2CRegisterType::Data, value);
                }
                self.xfer_last_byte(false);
                self.xfer_byte(false);
                self.wait_busy();
                self.halt_xfer();
                if self.operation_result().is_ok() {
                    return Ok(());
                }
            }
            self.halt_xfer();
            self.wait_busy();
        }
        Err(())
    }
}

/// Register type of the `Device`s.
#[derive(Debug, Clone, Copy)]
#[repr(u32)]
pub enum I2CRegisterType {
    Data  = 0x00,
    Cnt   = 0x01,
    Cntex = 0x02,
    Scl   = 0x04,
}

/// `CNT` information in a more structured form.
#[derive(Debug, Clone, Copy)]
pub struct CntInfo(u8);

impl CntInfo {
    /// Returns `true` if `CNT` register is in stop state
    pub fn is_stop(self) -> bool { (1 & (self.0 >> 6)) == 1 }

    /// Returns `true` if `CNT` register is in start state
    pub fn is_start(self) -> bool { (1 & (self.0 >> 5)) == 1 }

    /// Returns `true` if `CNT` register is in pause state
    pub fn is_pause(self) -> bool { (1 & (self.0 >> 4)) == 1 }

    /// Get `CNT` `Ack` state.
    pub fn ack(self) -> Ack { if (1 & (self.0 >> 3)) == 0 { Ack::Error } else { Ack::Ok } }

    /// Get `CNT` data direction
    pub fn data_direction(self) -> DataDirection {
        if (1 & (self.0 >> 2)) == 0 { DataDirection::Write } else { DataDirection::Read }
    }

    /// Returns `true` if `CNT` register have interrupts enabled.
    pub fn is_interrupt_enable(self) -> bool { if (1 & (self.0 >> 1)) == 0 { false } else { true } }

    /// Returns `true` if the `CNT` state is busy (a.k.a. running).
    pub fn is_running(self) -> bool { (1 & self.0) == 1 }
}

/// `CNT` data direction.
///
/// It can be either `Write` or `Read`.
#[derive(Debug, Copy, Clone)]
#[repr(u8)]
pub enum DataDirection {
    Write = 0,
    Read  = 1,
}

/// `CNT` ack representation.
///
/// It can be either `Error` or `Ok`.
#[derive(Debug, Copy, Clone)]
#[repr(u8)]
pub enum Ack {
    Error = 0,
    Ok    = 1,
}
