//! Interrupt Request Line (IRQ) API
//!
//! Detailed information on [3dbrew](https://www.3dbrew.org/wiki/IRQ_Registers)
use crate::io::Register;

pub mod interrupts;

/// Base address for IRQ registers
pub const IRQ_BASE: u32 = 0x10001000u32;

/// IRQ registers variants
#[derive(Debug, Clone, Copy)]
#[repr(u32)]
pub enum IrqRegister {
    /// IRQ_IE
    Enabled = 0x00,
    /// IRQ_IF
    Pending = 0x04,
}

impl IrqRegister {
    /// Read the register as u32;
    pub fn read(&self) -> u32 { self.read_as_u32() }

    /// Set the `interrupts` to enabled state.
    pub fn enable(&self, interrupts: u32) {
        unsafe {
            self.write_as_u32(self.read() | interrupts);
        }
    }

    /// Set the `interrupts` to disabled state.
    pub fn disable(&self, interrupts: u32) {
        unsafe {
            self.write_as_u32(self.read() & !interrupts);
        }
    }

    /// Clear specifics `interrupts`
    pub fn clear(&self, interrupts: u32) {
        unsafe {
            self.write_as_u32(interrupts);
        }
    }

    /// Clear all register interrupts
    pub fn clear_all(&self) {
        unsafe {
            self.write_as_u32(!0);
        }
    }
}

impl Register for IrqRegister {
    fn address(&self) -> u32 { IRQ_BASE + (*self as u32) }
}
