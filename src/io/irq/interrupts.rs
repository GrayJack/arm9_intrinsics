//! IRQ interrupts documented here: https://www.3dbrew.org/wiki/IRQ_Registers

pub const DMAC_1_0: u32 = 1 << 0;
pub const DMAC_1_1: u32 = 1 << 1;
pub const DMAC_1_2: u32 = 1 << 2;
pub const DMAC_1_3: u32 = 1 << 3;
pub const DMAC_1_4: u32 = 1 << 4;
pub const DMAC_1_5: u32 = 1 << 5;
pub const DMAC_1_6: u32 = 1 << 6;
pub const DMAC_1_7: u32 = 1 << 7;
pub const TIMER_0: u32 = 1 << 8;
pub const TIMER_1: u32 = 1 << 9;
pub const TIMER_2: u32 = 1 << 10;
pub const TIMER_3: u32 = 1 << 11;
pub const PXI_SYNC: u32 = 1 << 12;
pub const PXI_NOT_FULL: u32 = 1 << 13;
pub const PXI_NOT_EMPTY: u32 = 1 << 14;
pub const AES: u32 = 1 << 15;
pub const SDIO_1: u32 = 1 << 16;
pub const SDIO_1_ASYNC: u32 = 1 << 17;
pub const SDIO_3: u32 = 1 << 18;
pub const SDIO_3_ASYNC: u32 = 1 << 19;
pub const DEBUG_RECV: u32 = 1 << 20;
pub const DEBUG_SEND: u32 = 1 << 21;
pub const RSA: u32 = 1 << 22;
pub const CTR_CARD_1: u32 = 1 << 23;
pub const CTR_CARD_2: u32 = 1 << 24;
pub const CGC: u32 = 1 << 25;
pub const CGC_DET: u32 = 1 << 26;
pub const DS_CARD: u32 = 1 << 27;
pub const DMAC_2: u32 = 1 << 28;
pub const DMAC_2_ABORT: u32 = 1 << 29;

/// Enum representation of the IRQ Interrupt.
/// Dunno if I'll keep that
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum IrqInterrupt {
    Dmac1_0 = DMAC_1_0,
    Dmac1_1 = DMAC_1_1,
    Dmac1_2 = DMAC_1_2,
    Dmac1_3 = DMAC_1_3,
    Dmac1_4 = DMAC_1_4,
    Dmac1_5 = DMAC_1_5,
    Dmac1_6 = DMAC_1_6,
    Dmac1_7 = DMAC_1_7,
    Timer0 = TIMER_0,
    Timer1 = TIMER_1,
    Timer2 = TIMER_2,
    Timer3 = TIMER_3,
    PxiSync = PXI_SYNC,
    PxiNotFull = PXI_NOT_FULL,
    PxiNotEmpty = PXI_NOT_EMPTY,
    Aes    = AES,
    Sdio1  = SDIO_1,
    Sdio1Async = SDIO_1_ASYNC,
    Sdio3  = SDIO_3,
    Sdio3Async = SDIO_3_ASYNC,
    DebugRecv = DEBUG_RECV,
    DebugSend = DEBUG_SEND,
    Rsa    = RSA,
    CtrCard1 = CTR_CARD_1,
    CtrCard2 = CTR_CARD_2,
    Cgc    = CGC,
    CgcDet = CGC_DET,
    DSCard = DS_CARD,
    Dmac2  = DMAC_2,
    Dmac2Abort = DMAC_2_ABORT,
}
