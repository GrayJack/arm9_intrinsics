//! Traits, helpers, and type definitions for core I/O functionality on 3DS ARM9 land.
//!
//! The `arm9_intrisics::io` module contains a number of common things you'll need when
//! doing input and output.
pub mod i2c;
pub mod irq;

/// Specify that a object (usually `enum`) is a register in memory.
pub trait Register {
    /// The address of the register.
    fn address(&self) -> u32;

    /// Returns a mutable pointer of a `Register` object.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `T`
    fn as_mut_ptr<T: Copy>(&self) -> *mut T {
        assert!((self.address() as usize) & (core::mem::align_of::<T>() - 1) == 0);
        self.address() as *mut T
    }

    /// Read the register as `u8`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u8`
    fn read_as_u8(&self) -> u8 { unsafe { self.as_mut_ptr::<u8>().read_volatile() } }

    /// Read the register as `u16`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u16`
    fn read_as_u16(&self) -> u16 { unsafe { self.as_mut_ptr::<u16>().read_volatile() } }

    /// Read the register as `u32`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u32`
    fn read_as_u32(&self) -> u32 { unsafe { self.as_mut_ptr::<u32>().read_volatile() } }

    /// Write to the register considering the register size as `u8`.
    ///
    /// # Panics
    ///
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u8`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u8(&self, val: u8) { self.as_mut_ptr::<u8>().write_volatile(val); }

    /// Write to the register considering the register size as `u16`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u16`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u16(&self, val: u16) { self.as_mut_ptr::<u16>().write_volatile(val); }

    /// Write to the register considering the register size as `u32`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u32`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u32(&self, val: u32) { self.as_mut_ptr::<u32>().write_volatile(val); }
}
