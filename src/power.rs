//! Module with helper functions to do power management tasks
use crate::io::i2c::Device;

/// Reboot the system.
pub fn reboot() -> ! {
    loop {
        Device::Mcu1.write_byte(0x20, 0x4).unwrap()
    }
}

/// Power off the system.
pub fn power_off() -> ! {
    loop {
        Device::Mcu1.write_byte(0x20, 0x1).unwrap()
    }
}
